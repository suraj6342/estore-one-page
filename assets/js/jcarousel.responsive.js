(function($) {
    $(function() {
        var jcarousel = $('#category-carousel');
        var jcarousel1 = $('#featured-carousel');
        var jcarousel3 = $('#brands-carousel');
        var jcarousel4 = $('#about-carousel');


        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 769) {
                    width = width / 7;
                } else if (width <=560){
                    width = width / 3;
                } else{
                     width = width / 5;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        jcarousel1
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 800) {
                    width = width / 4;
                } else if (width <=560){
                    width = width / 2;
                } else{
                     width = width / 3;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

         jcarousel3
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel3_tab = $(this),
                    width = carousel3_tab.innerWidth();

                if (width >= 800) {
                    width = width / 5;
                } else if (width <= 560){
                    width = width / 2;
                } else{
                     width = width / 4;
                }

                carousel3_tab.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        jcarousel4
        .on('jcarousel:reload jcarousel:create', function () {
            var carousel3_tab = $(this),
                width = carousel3_tab.innerWidth();

            if (width >= 800) {
                width = width / 1;
            } else if (width <= 560){
                width = width / 1;
            } else{
                 width = width / 1;
            }

            carousel3_tab.jcarousel('items').css('width', Math.ceil(width) + 'px');
        })
        .jcarousel({
            wrap: 'circular'
        });


        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });
        
        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);
