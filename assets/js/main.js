(function ($)
  { "use strict"
  
/* 1. Proloder */
    $(window).on('load', function () {
      $('#preloader-active').delay(450).fadeOut('slow');
      $('body').delay(450).css({
        'overflow': 'visible'
      });
    });


/* 9. data-background */
    $("[data-background]").each(function () {
      $(this).css("background-image", "url(" + $(this).attr("data-background") + ")")
      });

    
// 12 Pop Up Img
    var popUp = $('.single_gallery_part, .img-pop-up');
      if(popUp.length){
        popUp.magnificPopup({
          type: 'image',
          gallery:{
            enabled:true
          }
        });
      }




  //product list slider
  var product_list_slider = $('.product_list_slider');
  if (product_list_slider.length) {
    product_list_slider.owlCarousel({
      items: 1,
      loop: true,
      dots: false,
      autoplay: true,
      autoplayHoverPause: true,
      autoplayTimeout: 5000,
            nav: true,
            navText: ["next", "previous"],
            smartSpeed: 1000,
            responsive: {
              0: {
                margin: 15,
                nav: false,
                items: 1
              },
              600: {
                margin: 15,
                items: 1,
                nav: false
              },
              768: {
                margin: 30,
                nav: true,
                items: 1
              }
            }
          });
        }

        if ($('.img-gal').length > 0) {
          $('.img-gal').magnificPopup({
            type: 'image',
            gallery: {
              enabled: true
            }
          });
        }
        
        // // menu fixed js code
        // $(window).scroll(function () {
        //   var window_top = $(window).scrollTop() + 1;
        //   if (window_top > 50) {
        //     $('.main_menu').addClass('menu_fixed animated fadeInDown');
        //   } else {
        //     $('.main_menu').removeClass('menu_fixed animated fadeInDown');
        //   }
        // });



        // Search Toggle
        $("#search_input_box").hide();
        $("#search_1").on("click", function () {
          $("#search_input_box").slideToggle();
          $("#search_input").focus();
        });
        $("#close_search").on("click", function () {
          $('#search_input_box').slideUp(500);
        });



      $('.select_option_dropdown').hide();
      $(".select_option_list").click(function () {
        $(this).parent(".select_option").children(".select_option_dropdown").slideToggle('100');
        $(this).find(".right").toggleClass("fas fa-caret-down, fas fa-caret-up");
      });

      if ($('.new_arrival_iner').length > 0) {
        var containerEl = document.querySelector('.new_arrival_iner');
        var mixer = mixitup(containerEl);
      }


      $('.controls').on('click', function(){
        $(this).addClass('active').siblings().removeClass('active');
      }); 


/* ----------------- Other Inner page End ------------------ */


})(jQuery);
